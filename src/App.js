import axios from 'axios';
import React, { useEffect, useState, useCallback } from 'react';

import './App.css';
import Header from './shared/components/Header.js';
import Sidebar from './shared/components/Sidebar';
import PostsContainer from './shared/widget/PostsContainer';

import PAYLOAD from './shared/payload.js';

const API_ENDPOINT = 'https://api.producthunt.com/v2/api/graphql';

function App() {
  const [posts, setPosts] = useState([]);

  const loadPostsData = useCallback((keyword = null) => {
    const payload = PAYLOAD;
    const config = {
      headers: {
        "Authorization": "Bearer eJy74inrPyMJnhiQUyFX14lkX1Zf7XmYXPAWWuG8DrE",
      }
    }
    return axios.post(API_ENDPOINT, payload, config).then(res => res.data).then(result => {
      const post_data = result?.data?.posts?.edges;
      if(post_data) {
        setPosts(post_data);
      }
    })
  }, [])

  useEffect(() => {
    loadPostsData();
  }, []);
  return (
    <div className="App">
      <Header searchData={loadPostsData} />
      <div className="container">
        <Sidebar />
        <PostsContainer data={posts}></PostsContainer>
      </div>
    </div>
  );
}

export default App;
