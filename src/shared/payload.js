const payload = {
	"query":"{ posts { edges { cursor node { commentsCount createdAt description featuredAt id isCollected isVoted media { type url videoUrl } name productLinks { type url } reviewsCount reviewsRating slug tagline thumbnail { type url videoUrl } url userId votesCount website } } pageInfo{ endCursor startCursor hasPreviousPage hasNextPage } totalCount } }"
}

export default payload;