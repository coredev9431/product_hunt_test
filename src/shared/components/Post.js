import moment from 'moment';
import React from 'react';
import { FaComment, FaHeart, FaRetweet } from 'react-icons/fa';

function Post(props) {
    const { data } = props;
    return (
        <div className="post-container">
            <img src={data.node.thumbnail.url} className="logo-container" />
            <div className="post-content">
                <a href={ data.node.url ? data.node.url : "#" } className="post-title" target="_blank">{data.node.name} @ <span className="time-part">{moment(data.node.createdAt).fromNow()}</span></a>
                <p className="post-description">{data.node.description}</p>
                <div className="post-footer">
                    <div className="post-btn">
                        <FaComment />
                        <span className="count-label">{data.node.commentsCount}</span>
                    </div>
                    <div className="post-btn">
                        <FaHeart />
                        <span className="count-label">{data.node.votesCount}</span>
                    </div>
                    <div className="post-btn">
                        <FaRetweet />
                        <span className="count-label">{data.node.reviewsCount}</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Post;