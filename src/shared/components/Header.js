import React, { useState } from 'react';
import logo from '../../logo.svg';
import '../../App.css';

function Header(props) {
    const { searchData } = props;
    const [search_keyword, setKeyword] = useState('');

    const keyUpEventProcess = (evt) => {
        if (evt.keyCode === 13) {
            if(searchData) {
                searchData(search_keyword);
            }
        }
    }

    return (
        <header className="App-header">
            <div className="logo">
                <img src={logo} className="App-logo" alt="logo" />
                <p>
                    TEST
                </p>
            </div>
            <div className="search-container">
                <input type="text" className="search-input" placeholder="Search..." onKeyUp={keyUpEventProcess} defaultValue={search_keyword} />
            </div>
        </header>
    )
}

export default Header;