import React from 'react';
import '../../App.css';

function Sidebar() {
    return (
        <div className="side-menu-container">
            <div className="side-menu-item">
                <div className="icon-container">#</div>
                <div className="label-container">Explorer</div>
            </div>
            <div className="side-menu-item">
                <div className="icon-container">#</div>
                <div className="label-container">Settings</div>
            </div>
        </div>
    );
}

export default Sidebar;