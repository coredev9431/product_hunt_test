import React from 'react';
import Post from '../components/Post';

function PostsContainer(props) {
    const { data } = props;
    return (
        <div className="content-container">
            {data.map((item, index) => (
                <Post key={index} data={item} />
            ))}
        </div>
    )
}

export default PostsContainer;